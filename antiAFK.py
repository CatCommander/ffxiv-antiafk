from pyautogui import keyDown, keyUp, press, write
from time import time, sleep
from random import randrange, uniform, choice
from datetime import datetime

# List of emotes, some intentionally misspelled just because
emotes = [
    "boxstep", 
    "laugh",
    "dance",
    "respect",
    "rezpect",
    "pray",
    "cheeron",
    "cheerjump",
    "cheeroon",
    "balldance",
]

# Create list of keys to hold while hiting hotbar actions
# I'm not using alt because I use consumables with that button :)
keys = [
    "ctrlleft",
    "shiftleft",
    "s",
    "d"
]

# Create function to cast a spell
# This is so we can do multiple casts because we sometimes practice weaving
def cast_0 (hold_time, button1):
    start = time()

    # While current time minus start time is less than hold time, do the stuff
    while time() - start < hold_time:

        # Select key from list of keys created above
        key = choice(keys)

        # Hold down the key
        keyDown(key)

        # Create a string of buttons so we can weave
        buttons = button1 + button1 + button1

        # Cast the spell 3 times every 1-3 seconds
        write(buttons, interval=randrange(1, 3))

        # Releasing key we're holding down because we're done casting
        keyUp(key)

# Create never-ending loop that casts and emotes
# To stop this, you will need to ctrl+c in the window that this script is running
while True:
    # Start loop, initial sleep to bring up FFXIV
    print("Starting loop!")

    # Get number for spell to cast
    numbah = str(randrange(1, 8))
    print("I will cast spell", numbah)

    # Pick emote from list
    emote  = choice(emotes)

    # Create emote string
    thing  = '/' + str(emote)
    print("I will be using the", thing, "emote")

    # Initial sleep
    print("I think I'll take a 1-10 second nap")
    sleep(randrange(1, 10))

    # Cast a random-ish spell
    print("I'm awake! Time to cast spell ", numbah)
    cast_0(randrange(1, 3), numbah)

    # Spell cooldown
    print("Whew, I'm pooped. I need to take a 5 - 10 second nap!")
    sleep(randrange(5, 10))

    # Do an emote
    print("Alright, I'm awake! Time to ", thing, "!")
    write(thing, interval=0.25)
    press('enter')

    # Emote cooldown and end of loop
    bigSleep = randrange(10, 1500)
    print("Whew, I'm pooped. I need to take a 10 second nap.. or maybe a", bigSleep, "second nap!")
    sleep(bigSleep)