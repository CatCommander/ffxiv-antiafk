# FFXIV AntiAFK


## Purpose
Queues are crazy out there right now and I got tired of having to wait for hours on end after work. I wrote this script for 2 reasons:
1. To be able to log in early in the morning and ensure that I'd be able to play after work
1. I needed something to motivate me to start diving into Python

This script will automatically casts spells, does some emotes, and moves the camera around which should _hopefully_ prevent you from getting kicked for being AFK.


## Prerequisites
-  Log into FFXIV


## Recommendations
- When you're planning on being AFK, go to your room at the inn or at your FC house or something. Just don't do this out in the open


## Important Notes
- When you're done AFK'ing, end the script otherwise it will continue to run and enter in emotes, cast spells, etc.


## Getting started


### Windows
1. [Download the zip archive of this repo](https://gitlab.com/GregBruno/ffxiv-antiafk/-/archive/main/ffxiv-antiafk-main.zip)

1. [Download and install Python 3.10.x](https://www.python.org/downloads/) (I was running [3.10.0](https://www.python.org/downloads/release/python-3100/) whhile writing this, if you use anything else then YMMV)

1. Unzip the archive somewhere easy to find (i.e. in your Documents or Downloads) 

1. Copy the _full_ directory path to the ffxiv-antiafk folder (i.e. C:\Users\FooBar\Documents\ffxiv-antiafk)

1. Open Powershell again (or use the same window from the above step if it's still open) and navigate to the directory of the unarchived path:
    - Run `cd {paste full directory path to unzipped archive}`

1. Install PyAutoGUI version 0.9.53 via pip using the requirements.txt file from the repo
    - Copy/paste this command and run it:
        - `pip install -r requirements.txt` 

1. Run the script!
    - `.\antiAFK.py`

1. Alt+Tab into FFXIV
    - The script automatically enters in commands and such for emotes, it's super important that you go into FFXIV immediately after running the script otherwise it will start pressing keys on your keyboard and entering in emotes!


### Linux
If you're running FFXIV on Linux I assume you already know how to do all this stuff. 


### Mac
FFXIV runs on Mac?


## Done being AFK and wanting to play?
Close the Powershell window that is running the script and then play!


## To Do
- Find a way to run this only in the FFXIV process so you can do other stuff on your computer while the script runs

- If 2002 errors continue to happen while queued, add a flag that automates the login process so you can go do other things while waiting in the queue

- Add some exception/error handling, probably


## Moral Conflict
You may be asking yourself "Dude, isn't it like.. bad to kind of afk while others are waiting in the queue? You're taking a spot from someone that could be playing!"

While I agree that I'm being a jerk to other folks by writing this and using it, I'm also paying a monthly subscription for a game I can't currently play because of the crazy long queues. 

If I _do_ get time to sit in a queue, I usually get disconnected with the ol' 2002 or 3001 and by the time I log back in, I've already lost my spot in the queue. Depending on the time of day, the queue can be anywhere from 1,000 - 6,000 deep which seems to be about 1hr per 1,000.

I'm very sympathetic for the devs/engineering teams working behind the scenes but history shows that big hyped up expansions bring lots and _lots_ of people. I _guarantee_ they scaled up in preparation for the latest expansion but they clearly didn't scale up enough.
